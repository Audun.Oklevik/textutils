package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class TestTest {
	TextAligner aligner = new TextAligner() {



		public String center(String text, int width) {
			if (text.length() > width){
				return text;
			}
			int extra = (width - text.trim().length()) / 2;
			StringBuilder resultString = new StringBuilder(" ".repeat(extra) + text.trim() + " ".repeat(extra));

			while (resultString.length() != width){
				resultString.append(" ");
				if (resultString.length() != width){
					resultString.insert(0, " ");
				}
			}

			return resultString.toString();

		}

		public String flushRight(String text, int width) {
			if (text.length() > width){
				return text;
			}
			int extra = (width - text.trim().length());
			return " ".repeat(extra) + text.trim();
		}

		public String flushLeft(String text, int width) {

			if (text.length() > width){
				return text;
			}

			int extra = (width - text.trim().length());
			System.out.println("Extra: " + extra);
			System.out.println("Length: " + text.length());
			return text.trim() + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			int whiteSpaces = (width - text.length());
			// Taken from https://stackoverflow.com/questions/4674850/converting-a-sentence-string-to-a-string-array-of-words-in-java
			String[] words = text.trim().split("\\s+");

			if (text.length() > width || words.length - 1 == 0){
				return center(text, width);
			}

			for (int i = 0; i < words.length; i++) {
				// You may want to check for a non-word character before blindly
				// performing a replacement
				// It may also be necessary to adjust the character class
				words[i] = words[i].replaceAll("[^\\w]", "");
			}

			StringBuilder resultString = new StringBuilder();

			for(char c : text.toCharArray()) {
				if (c == ' '){
					whiteSpaces ++;
				}
			}

			int rest = whiteSpaces % (words.length - 1);
			int iterations = whiteSpaces - rest;
			System.out.println(rest);

			for (String word : words) {
				if (!resultString.toString().equals("")) {
					resultString.append(" ".repeat(iterations / (words.length -1)));
					if (rest != 0) {
						resultString.append(" ");
						rest -= 1;
					}
				}
				resultString.append(word);
			}

			return resultString.toString();
		}
	};
		
	@Test
	void test() {
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("foo", aligner.center("foo", 1));
		assertEquals(" foo  ", aligner.center("foo", 6));
		assertEquals(" fee ", aligner.justify("  fee", 5));
	}

	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("  foo", aligner.flushRight("foo", 5));
		assertEquals("foo", aligner.flushRight("foo", 1));
		assertEquals("   foo", aligner.flushRight(" foo ", 6));
	}

	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
		assertEquals("foo", aligner.flushLeft("foo", 1));
		assertEquals("foo   ", aligner.flushLeft(" foo ", 6));
	}

	@Test
	void testJustify() {
		assertEquals("  A  ", aligner.justify("A", 5));
		assertEquals(" foo ", aligner.justify("foo", 5));
		assertEquals("foo", aligner.justify("foo", 1));
		assertEquals("f  o  o", aligner.justify("f o   o", 7));
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("fee    fie   foo", aligner.justify("fee fie foo", 16));
		assertEquals("feefiefoo", aligner.justify("feefiefoo", 3));
		assertEquals(" feefiefoo ", aligner.justify("  feefiefoo", 11));
	}
}
